/**Bài tập 1
 * Input: lương 100000/ngày, số ngày làm
 * 
 * Các bước xử lí:
 * 
 * Output: lương
 */
var soNgayLam = 30;
var luongMotNgay = 100000;
var luongThang = soNgayLam * luongMotNgay;
console.log("luongThang", luongThang);

/**Bài tập 2
 * Input: 5 số thực: 56789
 * 
 * Các bước xử lí:
 * 
 * Output: trung bình cộng của 5 số trên
 */
var n = 56789;
var hangDonvi = Math.floor((n % 100) % 10);
var hangChuc = Math.floor((n % 100) / 10);
var hangTram = Math.floor(n / 100) % 10;
var hangNghin =Math.floor((Math.floor(n / 100) % 100) / 10);
var hangChucNghin = Math.floor(Math.floor(n / 100) / 100);
var tongNamSo = hangDonvi + hangChuc + hangTram + hangNghin + hangChucNghin;
var trungBinhCong = tongNamSo / 5;
console.log("trung binh cong", trungBinhCong);
/**Bài tập 3
 * Input: số tiền USD
 * 
 * Các bước xử lí: 
 * 
 * Output: số tiền VND
 */
var tienDo = 23500;
var tienCanDoi = 100;
var quyDoi = tienDo * tienCanDoi;
console.log("tiền sau quy đổi", quyDoi);

/**Bài tập 4
 * Input: chiều dài, chiều rộng của hình chữ nhật
 * 
 * Các bước xử lí:
 * 
 * Output: chu vi, diện tích hình chữ nhật
 */
var chieuDai = 8;
var chieuRong = 5;
var chuVi = (chieuDai + chieuRong)*2;
var dienTich = chieuDai * chieuRong;
console.log("chu vi", chuVi);
console.log("dien tich", dienTich);

/**Bài tập 5
 * Input: 1 số có 2 chữ số
 * 
 * Các bước xử lí:
 * 
 * Output: tổng 2 chữ số
 */
var a = 69;
var hangDonvi = a % 10;
var hangChuc = Math.floor(a / 10);
var tong = hangChuc + hangDonvi;
console.log("tong 2 chu so", tong);
